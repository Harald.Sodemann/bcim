function aHk=alphaHk(T)
% Kinetic fractionation factor for 2H
% Input: T in [°C.]

% For details see Graf 2017 (ETH Diss No 24777)

% Merlivat 1978 (theoretical calculations)
DDhva=1.0251;

if T>=0
    Si=satvappress(T)/satvappress_ice(T);
elseif T<-23
    %Si=1.02-0.0038*T(i); % Petit et al. (1991)
    Si=1-0.004*T; % Jouzel and Merlivat (1984)
else
    Si=interp1([-30,-23,0,10],[1.12,1.092,1.001,0.9084],T,'pchip');
end

% Jouzel and Merlivat 1984:
aHk=Si./(alphaHs(T).*DDhva.*(Si-1)+1);

end