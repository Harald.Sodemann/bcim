function [varargout]=get_profile_value(prof,height,varargin)
% Get interpolated value of the variables given as "varargin" at "height".
% Names in varargin must correspont to the field names in struct "prof"

varargout=cell(size(varargin));
for i=1:length(varargin)
    varargout{i}=interp1(prof.z,prof.(varargin{i}),height);
end
end