function esat=satvappress(T)
% Calculate saturation vapour pressure of vapour over liquid
% Temperature in °C.
% Result in Pa

% from Murphy and Koop (2005)

% Coefficients
A=54.842763;
B=6763.22;
C=4.210;
D=0.000367;
E=0.0415;
F=218.8;
G=53.878;
H=1331.22;
I=9.44523;
J=0.014025;

T=T+273.15; % Change temperature to Kelvin

esat=exp(A-B./T-C*log(T)+D*T+tanh(E*(T-F)).*(G-H./T-I*log(T)+J*T)); % for 123 K < T < 332 K


% Alternative formula (from other publication)
% A=17.625;
% B=243.04;
% C=610.94;
% 
% esat=C*exp(A*T./(B+T));
end