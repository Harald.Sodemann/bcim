function L=latheat_sublim(T)
% Function to calculate the latent heat of sublimation
% T:    Temperature [°C]

% Rogers and Yau (1989)
L=2834100-290*T+4*T^2; % For -40°C. < T < 0°C.

% List (1984)
% L=2.83458e6-T.*(340+10.46*T); % For -40°C. < T < 0°C.

end