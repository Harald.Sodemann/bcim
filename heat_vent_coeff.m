function fh=heat_vent_coeff(diameter,T,press,rh)
% Function to calculate the heat ventilation coefficient
% diameter: drop diameter [m]
% T:        Temperature [°C]
% press:    pressure [Pa]
% rh:       relative humidity []

% After Pruppacher and Klett, 2010

cp = 1005;  % specific heat of dry air at constant pressure
rho=air_density(T,press,rh); % air density
Re=terminal_velocity(diameter,press,T)*diameter*rho/dynamic_viscosity(T); % Reynolds number
kappa=therm_cond(T,rh,press)/(rho*cp);
Pr=dynamic_viscosity(T)/(rho*kappa); % Prandtl number
PrRe=Pr^(1/3)*sqrt(Re);
if PrRe<1.4
    fh=1.0 + 0.108 * PrRe^2;
else
    fh=0.78 + 0.308 * PrRe;
end

end