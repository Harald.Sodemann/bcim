function [deltaHp,deltaOp,deltaHp_eq,deltaOp_eq,profiles]=calc_isotopes(profiles,namelist,const,outarray)

% Define ratios of the diffusion constants

% from Cappa (2003)
% DDhva=1.016;
% DDova=1.032;
% from Merlivat (1978) -> theoretical calculations
DDhva=1.0251;
DDova=1.0285;

nexp=0.58; % Empirical exponent (turbulent vs diffusive transport, c.f., Stewart (1975)
DDh=DDhva.^nexp; % for 2H
DDo=DDova.^nexp; % for 18O

% Calculate the remaining mass fractions
% Remaining fraction relative to the initial drop mass
ftot(length(outarray))=1;
for i=1:length(outarray)
    ftot(i)=profiles.m(i)/profiles.m(length(outarray));
end
% Remaining fraction relative to the mass at the next height
f(length(outarray))=1;
for i=length(outarray)-1:-1:1
    f(i)=ftot(i)/ftot(i+1); % using m instead of ftot leads to the same result
end

profiles.f=f;
profiles.ftot=ftot;

% Define which temperature is used for the isotopic calculations (Tint)
if strcmp(namelist.fract_T,'env') % Environmental temperature
    Tint=get_profile_value(profiles,outarray,'T');
elseif strcmp(namelist.fract_T,'drop') % Drop temperature
    Tint=profiles.Td;
elseif strcmp(namelist.fract_T,'mix') % Average between environmental and drop temperature
    Tint=(get_profile_value(profiles,outarray,'T')+profiles.Td)/2;
end

% Calculate isotopic ratios of the surrounding vapour
RHv=const.RHvsmow*(get_profile_value(profiles,outarray,'deltaHv')/1000+1);
ROv=const.ROvsmow*(get_profile_value(profiles,outarray,'deltaOv')/1000+1);

% Calculate initial isotopic composition if the hydrometeor
% ---------------------------------------------------------
if Tint(end)<0 % Initial hydrometeor temperature is negative: Choose formation mechanism
    switch namelist.formmech
        case 'depo' % Formation by vapour deposition on solid hydrometeor: use fractionation factors over ice
            RHp(length(outarray))=alphaHs(Tint(length(outarray)))*RHv(length(outarray));
            ROp(length(outarray))=alphaOs(Tint(length(outarray)))*ROv(length(outarray));
        case 'wbf' % Formation with the Wegener-Bergeron-Findeisen process
            if namelist.z_form>=0 % Formation height is defined: use values of Tint and RHv/ROv at that height
                RHp(length(outarray))=alphaHs(Tint(length(outarray)))*...
                    alphaHk(Tint(length(outarray)))*RHv(length(outarray));
                ROp(length(outarray))=alphaOs(Tint(length(outarray)))*...
                    alphaOk(Tint(length(outarray)))*ROv(length(outarray));
            else % Use weighted average values of Tint and RHv/ROv.
                 % The heigths are weighted according to their excess moisture
                RHp(length(outarray))=sum(alphaHs(profiles.T).*alphaHk(profiles.T).*...
                    const.RHvsmow.*(get_profile_value(profiles,profiles.z,'deltaHv')/1000+1).*...
                    profiles.exm)./sum(profiles.exm);
                ROp(length(outarray))=sum(alphaOs(profiles.T).*alphaOk(profiles.T).*...
                    const.ROvsmow.*(get_profile_value(profiles,profiles.z,'deltaOv')/1000+1).*...
                    profiles.exm)./sum(profiles.exm);
            end
        case 'riming' % Formation by riming of liquid drops (formed by fractionation over liquid)
                      % on the solid hydrometeor. Fraction of riming (versus solid deposition)
                      % is defined in namelist.rimingfrac
            if namelist.z_form>=0 % Formation height is defined: use values of Tint and RHv/ROv at that height
                RHp(length(outarray))=RHv(length(outarray))*...
                    ((1-namelist.rimingfrac)*alphaHs(Tint(length(outarray)))*alphaHk(Tint(length(outarray)))+...
                    namelist.rimingfrac*alphaH(Tint(length(outarray))));
                ROp(length(outarray))=ROv(length(outarray))*...
                    ((1-namelist.rimingfrac)*alphaOs(Tint(length(outarray)))*alphaOk(Tint(length(outarray)))+...
                    namelist.rimingfrac*alphaO(Tint(length(outarray))));
            else % Use weighted average values of Tint and RHv/ROv (see above)
                RHp(length(outarray))=sum(const.RHvsmow.*(get_profile_value(profiles,profiles.z,'deltaHv')/1000+1).*...
                    ((1-namelist.rimingfrac).*alphaHs(Tint).*alphaHk(Tint)+...
                    namelist.rimingfrac.*alphaH(Tint)).*profiles.exm)./sum(profiles.exm);
                ROp(length(outarray))=sum(const.ROvsmow.*(get_profile_value(profiles,profiles.z,'deltaOv')/1000+1).*...
                    ((1-namelist.rimingfrac).*alphaOs(Tint).*alphaOk(Tint)+...
                    namelist.rimingfrac.*alphaO(Tint)).*profiles.exm)./sum(profiles.exm);
            end
    end
else % Initial hydrometeor temperature is positive: Formation by condensation (with fractionation over liquid)
    RHp(length(outarray))=alphaH(Tint(length(outarray)))*RHv(length(outarray));
    ROp(length(outarray))=alphaO(Tint(length(outarray)))*ROv(length(outarray));
end

% Calculate isotopic composition of the hydrometeor for the rest of the profile
% -----------------------------------------------------------------------------

if strcmp(namelist.isocalc,'stewart') % Calculation according to Stewart (1975)

    % relative humidity of the environment with respect to the drop temperature
    rh=profiles.equih;

    % BO and BH are used below
    BO=alphaO(Tint).*DDo.*(1-rh);
    BH=alphaH(Tint).*DDh.*(1-rh);

    % (3e) in Stewart (1975)
    gammaO=alphaO(Tint).*rh./(1-BO);
    gammaH=alphaH(Tint).*rh./(1-BH);

    % (3d) in Stewart (1975)
    betaO=(1-BO)./(BO);
    betaH=(1-BH)./(BH);

    % Start calculation from top to bottom
    for i=length(outarray):-1:2
        if Tint(i)>0 && f(i)<=1 % Temperature positive and drop is shrinking (change of composition by growth is neglected)
            RHp(i-1)=gammaH(i)*RHv(i)+(RHp(i)-gammaH(i)*RHv(i))*f(i)^betaH(i); % From (2) in Stewart (1975)
            ROp(i-1)=gammaO(i)*ROv(i)+(ROp(i)-gammaO(i)*ROv(i))*f(i)^betaO(i);
        else
            RHp(i-1)=RHp(i); % No fractionation occurs: Keep previous RHp
            ROp(i-1)=ROp(i);
        end
    end

elseif strcmp(namelist.isocalc,'explicit') % Calculation using masses of all isotopic species rather than ratios

    % Calculate initial mass of all isotopic species from the isotopic
    % ratio
    mh2o(length(outarray))=(1-RHp(end)-ROp(end))*profiles.m(end);   % mass of HHO
    mhdo(length(outarray))=RHp(end)*profiles.m(end);                % mass of HDO
    m18o(length(outarray))=ROp(end)*profiles.m(end);                % mass of HH18O
    mtot(length(outarray))=profiles.m(end);                         % total mass

    mvaptot(length(outarray))=0;  % evaporated water mass between current and previous altitude
    RHvap(length(outarray))=0;    % composition of the evaporated water
    ROvap(length(outarray))=0;

    % Start calculation from top to bottom
    for i=length(outarray):-1:2
        % Determine average values of T, p and h between heights z(i-1) and z(i)
        [T(i),p(i),rh(i)]=get_profile_value(profiles,mean([outarray(i-1),outarray(i)]),'T','p','h');
        % Determine average value of Tint between heights z(i-1) and z(i)
        Tint(i)=mean([Tint(i),Tint(i-1)]);
        % Determine the time that the hydrometeor uses to dexcend from z(i) to z(i-1)
        dt=(outarray(i)-outarray(i-1))/terminal_velocity(profiles.diam(i),p(i),T(i));

        if Tint(i)>0  % Tint is positive: fractionation occurs
            % Determine some coefficients
            diff(i)=diffusion_coefficient(T(i),p(i));   % Diffusivity of the light isotope (H2O)

            % mass rate of change of the light isotope ( dm(H2O)/dt );
            % See Equations (3.11 and 3.22) in Graf 2017 (Diss ETH No. 24777)
            dmldt(i-1)=2*pi*profiles.diam(i)*diff(i)/const.Rw*...
                mass_vent_coeff(profiles.diam(i),T(i),p(i),rh(i))*...
                (rh(i)*satvappress(T(i))./(T(i)+273.15)*...
                (1-RHv(i)-ROv(i))-... % ratio of light isotope in vapour
                satvappress(Tint(i))./(Tint(i)+273.15)*...
                (1-RHp(i)/alphaH(Tint(i))-ROp(i)/alphaO(Tint(i))));% ratio of light isotope in hydrometeor

            mh2o(i-1)=mh2o(i)+dt*dmldt(i-1); % new mh2o

            % Define factor with all constants (for readability)
            F=2*pi*profiles.diam(i)*diff(i)*mass_vent_coeff(profiles.diam(i),T(i),p(i),rh(i))/const.Rw;
            % Same as above, but for m18o and mhdo (see Eq. (3.22) in Graf 2017)
            m18o(i-1)=m18o(i)+dt*F*(1./DDova)^nexp*(rh(i)*ROv(i)*satvappress(T(i))./(T(i)+273.15)-...
                ROp(i)/alphaO(Tint(i))*satvappress(Tint(i))./(Tint(i)+273.15));
            mhdo(i-1)=mhdo(i)+dt*F*(1./DDhva)^nexp*(rh(i)*RHv(i)*satvappress(T(i))./(T(i)+273.15)-...
                RHp(i)/alphaH(Tint(i))*satvappress(Tint(i))./(Tint(i)+273.15));

            % Total change of mass (Eq. 3.11)
            mtot(i-1)=mtot(i)+dt*2*pi*profiles.diam(i)*diff(i)/const.Rw*...
                mass_vent_coeff(profiles.diam(i),T(i),p(i),rh(i))*...
                (rh(i)*satvappress(T(i))./(T(i)+273.15)-...
                satvappress(Tint(i))./(Tint(i)+273.15));
            % Calculate new isotopic ratios
            RHp(i-1)=mhdo(i-1)/(m18o(i-1)+mhdo(i-1)+mh2o(i-1));
            ROp(i-1)=m18o(i-1)/(m18o(i-1)+mhdo(i-1)+mh2o(i-1));

            % Calculate evaporated mass
            mvaptot(i-1)=mtot(i)-mtot(i-1);
            % Calculate isotopic ratio of the evaporated mass
            RHvap(i-1)=(mhdo(i)-mhdo(i-1))/(m18o(i)-m18o(i-1)+mhdo(i)-mhdo(i-1)+mh2o(i)-mh2o(i-1));
            ROvap(i-1)=(m18o(i)-m18o(i-1))/(m18o(i)-m18o(i-1)+mhdo(i)-mhdo(i-1)+mh2o(i)-mh2o(i-1));

        else % Tint is not positive
            if profiles.m(i-1)-profiles.m(i)<=0 % mass decreases:
                % Relative change of mass is the same for all isotope species (no fractionation)
                mh2o(i-1)=(1-RHp(i)-ROp(i))*profiles.m(i-1);
                mhdo(i-1)=RHp(i)*profiles.m(i-1);
                m18o(i-1)=ROp(i)*profiles.m(i-1);
                mtot(i-1)=profiles.m(i-1);
                RHp(i-1)=RHp(i);
                ROp(i-1)=ROp(i);
                % Calculate properties of evaporated mass
                mvaptot(i-1)=mtot(i)-mtot(i-1);
                RHvap(i-1)=mvaptot(i-1)*RHp(i-1);
                ROvap(i-1)=mvaptot(i-1)*ROp(i-1);
            else % mass increases:
                % Fractionation occurs (deposition of vapour on ice)
                mh2o(i-1)=(1-RHp(i)-ROp(i))*profiles.m(i)+...
                    (profiles.m(i-1)-profiles.m(i))*...
                    (1-RHv(i-1)*alphaHs(Tint(i-1))-ROv(i-1)*alphaOs(Tint(i-1)));
                mhdo(i-1)=RHp(i)*profiles.m(i)+...
                    (profiles.m(i-1)-profiles.m(i))*...
                    RHv(i-1)*alphaHs(Tint(i-1));
                m18o(i-1)=ROp(i)*profiles.m(i)+...
                    (profiles.m(i-1)-profiles.m(i))*...
                    ROv(i-1)*alphaOs(Tint(i-1));
                mtot(i-1)=profiles.m(i-1);
                RHp(i-1)=mhdo(i-1)/(m18o(i-1)+mhdo(i-1)+mh2o(i-1));
                ROp(i-1)=m18o(i-1)/(m18o(i-1)+mhdo(i-1)+mh2o(i-1));
                % Calculate properties of deposited mass
                mvaptot(i-1)=mtot(i)-mtot(i-1);
                RHvap(i-1)=RHv(i-1)*alphaHs(Tint(i-1));
                ROvap(i-1)=ROv(i-1)*alphaOs(Tint(i-1));
            end
        end
    end
end

% Calculation isotopic ratio of equilibrium vapor from the hydrometeor
RHp_eq=zeros(size(RHp));
ROp_eq=zeros(size(RHp));
for i=1:length(outarray)
    if Tint(i)<0 % equilibrium vapor from solid hydrometeor
        RHp_eq(i)=RHp(i)./alphaHs(Tint(i));
        ROp_eq(i)=ROp(i)./alphaOs(Tint(i));
    else % equilibrium vapor from liquid hydrometeor
        RHp_eq(i)=RHp(i)./alphaH(Tint(i));
        ROp_eq(i)=ROp(i)./alphaO(Tint(i));
    end
end

% Calculate the delta values of the hydrometeor
deltaHp=(RHp./const.RHvsmow-1)*1000;
deltaOp=(ROp./const.ROvsmow-1)*1000;

% Calculation delta value of equilibrium vapor from the hydrometeor
deltaHp_eq=(RHp_eq./const.RHvsmow-1)*1000;
deltaOp_eq=(ROp_eq./const.ROvsmow-1)*1000;

% Add some variables to the output if namelist switch is on
if namelist.fulloutput==1
%     profiles.F=F;
%     profiles.qv=qv;
%     profiles.qv18o=qv18o;
%     profiles.qv2h=qv2h;
%     profiles.ql=ql;
%     profiles.ql18o=ql18o;
%     profiles.ql2h=ql2h;
%     profiles.z_q=z_q;
    profiles.mvaptot=mvaptot;
    profiles.RHvap=RHvap;
    profiles.ROvap=ROvap;
end
end
