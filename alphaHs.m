function aHs=alphaHs(T)
% Equilibrium fractionation factor over solid for 2H
% Input: T in [°C.]

% Merlivat&Nief'67
aHs=exp(16289./((T+273.15).^2)-0.0945);
end
