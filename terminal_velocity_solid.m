function vt=terminal_velocity_solid(diameter)
% Function to calculate the terminal velocity of a solid hydrometeor
% Input: Melted drop diameter in m
% Output: terminal velocity in m/s

% Langleben 1954:
% Terminal velocity of dendrite snowflakes with "melted diameter" of 0.4 - 3.5 mm:
% Original equation: vt=160*diameter^0.31 with diameter in cm and vt in cm/s
vt=1.60.*(diameter.*100).^0.3;

end