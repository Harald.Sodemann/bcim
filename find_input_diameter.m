function inputdiam=find_input_diameter(diamin,startheight,profiles,const,namelist)
% Function to calculate the initial diameter of a hydrometeor, if the
% terminal diameter was given.
% Inputs:
% Profiles of T,p and h from the starting height to the ground, with a vertical
% resolution of 1m, where index 1 refers to the starting height

iterthresh=1e-5; % iteration threshold in [m]. Interate until difference to
                 % target diam is smaller than iterthresh
fact=0.7;   % iteration factor
corrdir=1;  % variable for correction direction.
evap=0;     % variable to indicate if the drop has evaporated completely during the previous iteration
lastvalid=[]; % last valid index
diamout=0;  % output diameter

% Determine maximum number of vertical integration steps (itersteps) and integration step
% size (dt)
if namelist.dt==0 % height integration
    itersteps=startheight/namelist.dz;
    dt=namelist.dz;
else % time integration
    itersteps=20000/namelist.dt;
    dt=namelist.dt;
end

newdiam=diamin; % diameter for current iteration
iter=0; % iteration number

while abs(diamout-diamin)>iterthresh || evap==1 % continue iteration if
        % current terminal diameter is still not within the threshold of
        % target diameter or if the drop has evaporated completely
        
    height=zeros(itersteps,1);
    temptend=zeros(itersteps,1); % temperature tendency
    Td=zeros(itersteps,1);  % drop temperature
    masstend=zeros(itersteps,1); % mass tendency
    m=zeros(itersteps,1); % drop mass
    diam=zeros(itersteps,1); % drop diameter
    
    % Define initial values for vertical integration
    height(1)=startheight;
    Td(1)=get_profile_value(profiles,startheight,'T'); % Initial drop temperature equals
        % the temperature of the environment at the starting height
    diam(1)=newdiam;
    m(1)=1000/6*pi*diam(1)^3;
    
    iter=iter+1;
    i=1;
    while i<itersteps  % vertically integrate while maximum number of vertical itegration steps is not reached
        i=i+1;
        dt=diam(i-1)*500;  % Adapt integration step size to the diameter to avoid numerical instabilities
        
        % Get values of T and p at height(i-1) to calculate height(i)
        [T,p]=get_profile_value(profiles,height(i-1),'T','p');
        % Calculate height of next vertical integration step
        if namelist.dt==0 % height integration
            height(i)=height(i-1)-dt;
        else % time integration: Calculate new height by using the fall velocity of the drop
            if Td(i-1)<=0 && namelist.solidterminalvelocity==1
                % Use terminal velocity of a solid hydrometeor if Td<=0°C and
                % if this is allowed by the namelist switch
                height(i)=height(i-1)-terminal_velocity_solid(diam(i-1))*dt;
            else
                % Use terminal velocity of a liquid hydrometeor (with the drop
                % size of the previous integration step)
                height(i)=height(i-1)-terminal_velocity(diam(i-1),p,T)*dt;
            end
        end
        
        % Get values of T, p and h at the next height
        [T,p,h]=get_profile_value(profiles,height(i),'T','p','h');
        if isnan(T) % Drop arrived on the ground. Use surface values to allow one more integration step
            T=profiles.T(end);
            p=profiles.p(end);
            h=profiles.h(end);
        end
        
        % Calculate new drop temperature
        if namelist.drop_T==1 % Drop temperature is allowed to be different from the environment
            temptend(i)=drop_T_tendency(diam(i-1),T,Td(i-1),p,h,const,namelist); % drop temperature tendency
            Td(i)=Td(i-1)-dt*temptend(i); % new drop temperature
        else
            temptend(i)=0;
            Td(i)=T; % drop temperatures equals environmental temperature
        end
       
        % Calculate new drop mass and diameter
        masstend(i)=drop_mass_tendency(diam(i-1),T,Td(i-1),p,h,const,namelist);
        m(i)=m(i-1)-dt*masstend(i);
        diam(i)=(6*m(i)/(pi*1000))^(1/3);
        
        if diam(i)<=5e-5 % drop evaporated completely: threshold is set to avoid numerical instabilities
            %fprintf('Drop arrived after %d time steps at a height of %d m\n',i,height(i))
            diam(i)=0;
            evap=1; % set evaporation switch to 1
            lastvalid=i-1;
            diam=diam(1:i);
            break; % stop vertical integration (evap=1 will advance to the next iteration)
        elseif isnan(m(i))
            fprintf('Error! m(i) of height(i) is NaN\n')
        else % continue 
            if namelist.dt~=0 % time integration
                % Check if drop arrived on the ground
                if height(i)<=profiles.z(1)
                    %fprintf('Drop arrived on the ground after %d time steps\n',i)
                    % Save current index and diameter
                    evap=0;
                    lastvalid=i;
                    diam=diam(1:i);
                    break; % Stop vertical integration (to check if diam is within the threshold)
                end
                % Check if maximum number of iteration steps is exceeded
                if i==itersteps
                    fprintf('Error! Maximum number of integration steps reached before drop evaporated completely or arrived on the ground!\n')
                end
            end
            lastvalid=i;
            evap=0;
        end
    end
    
    % Define terminal diameter if drop has not evaporated completely
    if evap==0
        diamout=diam(lastvalid);
    else
        diamout=0;
    end
    
    if sign(diamin-diamout)~=corrdir % Check if the sign of the difference
        % between current and target diameter has changed (this happens
        % when the correction was too large
        fact=fact*0.5; % Reduce correction factor by 50%
        corrdir=-corrdir; % Change direction of correction
    end
    %fprintf('Iteration %d : D_{start} = %f mm   D_{end} = %f mm\n',iter,diam(1)*1000,diamout*1000)
    
    % Define new input diameter for next iteration
    newdiam=diam(1)+fact*(diamin-diamout);
end

% Current diameter is close enough to the target diameter: Save current
% diameter.
inputdiam=diam(1);
fprintf('Final input diameter: D_{start} = %f mm\n',inputdiam*1000)
end
