function [profiles]=below_cloud_model(diam_input,prof_deltaH,prof_dexc,prof_rh,prof_T,z,namelist)
% Below cloud interaction model to calculate the evolution of the isotopic
% composition of a single hydrometeor falling through a vertical column.
% =========================================================================
% Developed and tested by Pascal Graf 2016 - 2018
% pascal.graf@env.ethz.ch
% =========================================================================
%
% Inputs:
% =======
%
% diam_input:   Cell array (1x2), containing the equivalent diameter of the
%               hydrometeor in [m] and an indication whether this is the
%               diameter at the starting height 'start' or the diameter
%               when arriving at the surface 'end'. The diameter at the
%               starting height is iteratively determined in the latter
%               case.
%               Example:    diam_input={0.001,'end'}
%               Hydrometeor arrives on the ground with a diameter of 1 mm.
% prof_deltaH:  (1xn) array, containing the vertical delta2H profile of
%               environmental water vapour. If n=2, the profile is linearly
%               interpolated between the lowest and highest model level
%               (defined with the input variable z). If n=length(z), the
%               entire profile of vapour delta2H is defined. Values of
%               prof_deltaH should be given in [permil].
%               Dimension has to be (1x2) if namelist.profilecalc=1. Then,
%               only the first value will be used as starting value for the
%               Rayleigh ascent.
%               Example:    prof_deltaH=[-50 -300]
%               Linear delta2H profile with -50 permil at the surface
%               (z(1)) and -300 permil at the highest model level (z(end)).
% prof_dexc:    Same as prof_deltaH, but for deuterium-excess in [permil]
% prof_rh:      Same as prof_deltaH, but for relative humidity in []
%               (50% = 0.5)
% prof_T:       Same as prof_deltaH, but for air temperature in [°C.]
% z:            (1xn) array, containing the vertical height levels in [m],
%               on which the above arrays are defined and on which the
%               output (profiles) will be defined.
%               Example:    z=linspace(500,3500,3001)
%               Linear array of height levels from 500 m to 3500 m with
%               steps of 1 m: [500, 501, 502,... ,3500]
% namelist:     Structure of different model input parameters in the format
%               "namelist.fieldname". Possible fieldname's are:
%               drop_T:     =1: (default) allow drop temperature to be
%                               different from air temperature.
%                           =0: drop temperature equals air temperature
%               p0:         Pressure at the ground (z(1)) in [Pa]
%                           default = 1000 [hPa] = 100000 [Pa]
%               massincrease:   =1: (default) allow drop mass increase
%                               =0: only allow drop mass decrease
%               dt:         Time integration time step [s]
%                           =0: time integration disabled. Integrate after
%                               dz. dz is defined in its own namelist field
%                           =0.1: (default). Other values are also possible
%               profilecalc:    =1: (default) calculate profiles of T, rh,
%                               delta2H and dexc with a Rayleight ascent.
%                               Only the first value of prof_T, prof_rh,
%                               prof_delta2H and prof_dexc is taken to
%                               initiate the ascent at the ground. The
%                               rest of the profiles are calculated with
%                               the function "rayleigh_ascent", by (moist-)
%                               adiabatically lifting an air parcel. All
%                               condensed moisture is rained out
%                               immediately.
%                               =0: Use input profiles of T, rh, delta2H
%                               and dexc. If input profiles are of size
%                               (1x2), they are linearly interpolated
%                               between z(1) and z(end).
%               solidterminalvelocity:  =1: use separate terminal velocity
%                                       for solid hydrometeors.
%                                       =0= (default) use terminal velocity
%                                       of a liquid drop for both liquid
%                                       and solid hydrometeors.
%               fract_T:    Defines which temperature is used for the
%                           fractionation functions.
%                           'drop': (default) drop temperature
%                           'env': environmental air temperature
%                           'mix': average of 'drop' and 'env'
%               z_form:     height at which the hydrometeor is formed and
%                           where its fall is started.
%                           =0: (default) starting height is the highest
%                           model level (z(end)).
%                           =-1: starting height is at the lowermost level
%                           where the air is supersaturated with respect to
%                           ice (exm > 0).
%                           =3000: (for example) starting height is at 3000
%                           meters above sea level.
%               formmech:   Hydrometeor formation mechanism.
%                           'wbf': (default) Wegener-Bergeron-Findeisen
%                           process.
%                           'depo': vapour deposition on ice
%                           'riming': riming of liquid droples on ice.
%                           Corresponds to condensation of vapour on
%                           liquid. The degree of riming can be defined
%                           with namelist.rimingfrac (see below)
%               rimingfrac: Defines the degree (or fraction) of riming if
%                           namelist.formmech='riming'.
%                           Example:    namelist.rimingfrac=0.5 (default)
%                           50% of the hydrometeor mass is formed by
%                           riming, the remaining 50% are formed by the WBF
%                           process.
%               dz:         defines step size of the vertical integration
%                           and of the output in [m] if namelist.dt=0.
%                           Example:    namelist.dz=1 (default)
%                           Vertical resolution of integration and output
%                           is 1 m.
%               isocalc:    Defines how the isotopic composition of the
%                           hydrometeor is calculated.
%                           'explicit': (default) Calculate the isotopic
%                           composition using separate mass equations for
%                           all isotopic species.
%                           'stewart': Calculate the isotopic composition
%                           according to Stewart (1975) -> Equation (2).
%               temptendswitch: Switch evaporative cooling or adaptation to
%                               the environmental air temperature on or
%                               off.
%                               'both': (default) both switched on.
%                               'evap': Only evaporative cooling enabled.
%                               'adap': Only adaptation to environmental
%                               air temperature enabled.
%               constisotopeprofile:    =0: (default) Take the isotope
%                                       profile of vapour from the input or
%                                       calculate it with a Rayleigh
%                                       ascent, taking the lowermost values
%                                       of T, rh, delta2H and dexc, or take
%                                       the average of the lowest 100 m if
%                                       namelist.soundingprofile=1 (see
%                                       below)
%                                       =1: define the isotope profile of
%                                       vapour by a Rayleigh ascent with
%                                       starting values T=12°C and rh=0.75.
%                                       This can be useful to test the
%                                       sensitivity to temperature or
%                                       humidity profiles, but keeping the
%                                       isotope profile of vapour constant.
%               fulloutput: =0: (default) function "calc_isotopes" returns
%                           profiles of deltaHp, deltaOp, deltaHp_eq,
%                           deltaOp_eq, f and ftot.
%                           =1: function "calc_isotopes" additionally
%                           returns "mvaptot" (total evaporated vapour mass
%                           at each level), "RHvap" and "ROvap" (isotope
%                           ratios of the evaporated vapour)
%               soundingprofile:    =0: (default) input profiles of T and
%                                   rh are not from an atmospheric sounding
%                                   or should not be treated like one.
%                                   Rayleigh ascent uses the values of the
%                                   lowest index (e.g. T(1)) as starting
%                                   values.
%                                   =1: Take the average of the lowermost
%                                   100 m of the profiles of T and rh as
%                                   starting values for the Rayleigh
%                                   ascent. This should be switched on
%                                   when taking profiles of T and rh from
%                                   atmospheric soundings, where surface
%                                   effects can cause starting values which
%                                   are very different of the air layer
%                                   above and thus cause unrealistic
%                                   profiles.
%
% Outputs:
% ========
%
% This functions returns a struct "profiles" with the variable arrays
% listed below.
% Variables that describe the profile of ambient air are of size (1xn),
% where n is the length of the input array "z". "z" defines the grid on
% which these profiles are defined. For example:
%       z: (1x3001) array; p: (1x3001) array
%       z(305) = 805 m
%       p(305) = 93000 Pa
%       The pressure at model level 305 is 930 hPa, which corresponds to
%       805 m above sea level.
% Variables that describe properties of the hydrometeor are arrays of size
% (1xm), where m is the number of model levels, which are at and below the
% starting altitude (z_form) of the hydrometeor. The corresponding grid is
% defined in "z_out" (and "height"). For example:
%       z = 0:1:3000 -> (1x3001) array; z_form = 2000;
%       z_out contains all z<=z_form:
%       z_out = 0:1:2000 -> (1x2001) array
% m=n if z_form=z(end) (if namelist.z_form=0)
%
% z:        Height above sea level of all model levels [m]; (1xn) array.
% z_out:    Height above sea level of all model levels at and below the
%           formation height of the hydrometeor [m]; (1xm) array.
% height:   Same as z_out.
% h:        Relative humidity of ambient air []; (1xn) array.
% T:        Temperature of ambient air [°C]; (1xn) array.
% p:        Pressure of ambient air [Pa]; (1xn) array.
% exm:      Excess moisture of ambient air (specific humidity divided by
%           the saturation specific humidity over ice at ambient
%           temperature; exm>=0) [kg/kg]; (1xn) array.
%
% m:        Hydrometeor mass [kg]; (1xm) array.
% diam:     Hydrometeor diameter [m]; (1xm) array.
% Td:       Hydrometeor temperature [°C]; (1xm) array.
% Tdelta:   Temperature difference between hydrometeor and surrounding air
%           [°C]; (1xm) array.
% equih:    Relative humidity of the surrounding air with respect to Td,
%           instead of T (equivalent relative humidity) []; (1xm) array.
% masstend: Hydrometeor mass tendency (rate of change of mass dm/dt) [kg/s]
%           (1xm) array.
% temptend: Hydrometeor temperature tendency (rate of change of temperature
%           dTd/dt) [°C/s]; (1xm) array.
% f:        Remaining fraction of mass of the hydrometeor relative to the
%           next higher model level []; (1xm) array.
% ftot:     Remaining fraction of mass of the hydrometeor relative to the
%           initial mass (at z_form) []; (1xm) array.
% evapfrac: Evaporated fraction of mass (current mass relative to the
%           initial mass of the hydrometeor; evapfrac=1-ftot) [];
%           (1xm) array.
%
% deltaHv:  delta2H value of the ambient vapour [permil]; (1xn) array.
% deltaOv:  delta18O value of the ambient vapour [permil]; (1xn) array.
% dexcv: 	d-excess value of the ambient vapour [permil]; (1xn) array.
% deltaHp:  delta2H value of the hydrometeor [permil]; (1xm) array.
% dexcp:    d-excess value of the hydrometeor [permil]; (1xm) array.
% deltaHp_eq:   delta2H value of equilibrium vapour from the hydrometeor
%               at ambient temperature [permil]; (1xm) array.
% dexcp_eq:     d-excess value of equilibrium vapour from the hydrometeor
%               at ambient temperature [permil]; (1xm) array.
%
% z_intsteps:   Height above sea level of the individual integration steps
%               (different from z if time integration was chosen) [m];
%               array size corresponds to the number of integration steps.
% =========================================================================

% Change input profiles to type double
prof_deltaH=double(prof_deltaH);
prof_dexc=double(prof_dexc);
prof_T=double(prof_T);
prof_rh=double(prof_rh);
z=double(z);

% Read general model parameters from namelist or set default values
% -----------------------------------------------------------------

% if ~isfield(namelist,'solidfrac')
%     namelist.solidfrac=0;   % treat solid and liquid fractionation separately: 1=on
% end
if ~isfield(namelist,'drop_T')
    namelist.drop_T=1;      % drop temperature different from environmental T: 1=on
end
% if ~isfield(namelist,'satvapice')
%     namelist.satvapice=0;   % treat mass and T tendency for ice differently: 1=on
% end
if ~isfield(namelist,'p0')
    namelist.p0=100000;   % Pressure at the ground in Pa: default = 1000 hPa
end
if ~isfield(namelist,'massincrease')
    namelist.massincrease=1;   % Allow mass increase (dm/dt>0): 1=allow
end
if ~isfield(namelist,'dt')
    namelist.dt=0.1;  % Integration after dt: 0=off, else value of dt
end
if ~isfield(namelist,'profilecalc')
    namelist.profilecalc=1;   % allow calculation of input profile with a Rayleigh ascent: 1=allow
end
if ~isfield(namelist,'solidterminalvelocity')
    namelist.solidterminalvelocity=0;   % use separate terminal velocity for solid hydrometeors
end
if ~isfield(namelist,'fract_T')
    namelist.fract_T='drop';   % which temperature to use for fractionation:
    % 'env': environment; 'drop': drop;
    % 'mix':mean of drop and environment
end
if ~isfield(namelist,'z_form')
    namelist.z_form=0;   % height at which precipitation is formed
    % 0=on the top layer of the model
end
if ~isfield(namelist,'formmech')
    namelist.formmech='wbf';   % formation mechanism:
    % 'depo': deposition; 'wbf': wengener bergeron findeisen process
    % 'riming': riming - freezing of liquid droplets
    if strcmp(namelist.formmech,'riming') && ~isfield(namelist,'rimingfrac')
        namelist.rimingfrac=0.5; % fraction of precipitation formed by riming
        % rest of the precipitation will be formed by the WBF process
    end
end
if ~isfield(namelist,'dz')
    namelist.dz=1;  % set vertical resolution of the integration and output along z (if dt=0)
end
if ~isfield(namelist,'isocalc')
    namelist.isocalc='explicit';  % 'explicit' or 'stewart'
end
if ~isfield(namelist,'temptendswitch')
    namelist.temptendswitch='both';  % swich evaporative cooling or adaptation of the environmental
                              % temperature on and off. 'evap': evaporative
                              % cooling only; 'adap': no evaporative
                              % cooling; 'both': both on
end
if ~isfield(namelist,'constisotopeprofile')
    namelist.constisotopeprofile=0;   % take only the lowermost relative humidity value instead of the
                            % lowermost 100m for the relative humidity
                            % sensitivity experiments if =1.
                            % 0=normal calculation
end
if ~isfield(namelist,'fulloutput')
    namelist.fulloutput=0;
end
if ~isfield(namelist,'soundingprofile')
    namelist.soundingprofile=0;
end

% General constants
% -----------------
const.RHvsmow=155.76E-6;    % Isotopic ratio 2H/1H of VSMOW
const.ROvsmow=2005.20E-6;   % Isotopic ratio 18O/16O of VSMOW
const.R=8.31446;        % ideal gas constant
const.Rd=287.;       % specific gas constant for dry air
const.Rw=461.;       % specific gas constant for water
const.g=9.81;         % gravity constant
const.cw=4186;        % specific heat of water (almost constant above 0°C.)
const.ci=2068.6;      % specific heat of ice (at -5°C.) (Jacobson 1999)

% Define profiles for increasing height
% -------------------------------------
nstep=length(z);
profiles.z=z;

if nstep<=2
    fprintf('Error: No profile defined!\n')
    return;
end
% Raise error if diameter of drop arriving on the ground is given as input
% and if it's too small
if strcmp(diam_input{2},'end') && diam_input{1}<0.25e-3 && namelist.dt==0
    fprintf('Error: Final diameter too small!\n')
    return;
end

if namelist.profilecalc==0  % Check if Rayleigh ascent profile is allowed. Define linear profile if not
    % Define linear profile of deltaH if length of input (prof_deltaH) is 2
    if length(prof_deltaH)==2
        profiles.deltaHv=linspace(prof_deltaH(1),prof_deltaH(2),nstep);
    else
        % Raise error if input profile has not the same length as profile.z
        if length(prof_deltaH)~=nstep
            fprintf('Error: deltaH profile has the wrong size!\n')
            return;
        end
        % Take input profile if defined
        profiles.deltaHv=prof_deltaH;
    end
    % Same as above, but for dexc
    if length(prof_dexc)==2
        profiles.dexcv=linspace(prof_dexc(1),prof_dexc(2),nstep);
    else
        if length(prof_dexc)~=nstep
            fprintf('Error: d-excess profile has the wrong size!\n')
            return;
        end
        profiles.dexcv=prof_dexc;
    end
end


% Calculation of profiles
% -----------------------

% Calculate profiles (if not given)
if namelist.profilecalc==1 % Rayleight ascent
    if namelist.constisotopeprofile==1 % Rayleigh ascent with surface values T=12 and h=0.75
        [profiles.T,profiles.h,profiles.deltaHv,profiles.dexcv,~]=rayleigh_ascent(12,0.75,namelist.p0,prof_deltaH(1),prof_dexc(1),z);
    else
        if namelist.soundingprofile==1 % Take starting values of T and h from sounding
            % Average over the lowermost 100m of the profile to avoid
            % surface effects
            h0=mean(prof_rh(z<=(z(1)+100)));
            T0=mean(prof_T(z<=(z(1)+100)));
        else % Take lowermost value of the profiles of T and h
            h0=prof_rh(1);
            T0=prof_T(1);
        end
        % Calculate Rayleigh ascent with the above defined starting values
        [profiles.T,profiles.h,profiles.deltaHv,profiles.dexcv,~]=rayleigh_ascent(T0,h0,namelist.p0,prof_deltaH(1),prof_dexc(1),z);
    end
else
    % Define linear profile of h if two values are given, take input profile
    % if its length corresponds to the length of z
    if length(prof_rh)==2
        profiles.h=linspace(prof_rh(1),prof_rh(2),nstep);
    else
        if length(prof_rh)~=nstep
            fprintf('Error: RH profile has the wrong size!\n')
            return
        end
        profiles.h=prof_rh;
    end
    % Define linear profile of T if two values are given, take input profile
    % if its length corresponds to the length of z
    if length(prof_T)==2
        profiles.T=linspace(prof_T(1),prof_T(2),nstep);
    else
        if length(prof_T)~=nstep
            fprintf('Error: Temperature profile has the wrong size!\n')
            return
        end
        profiles.T=prof_T;
    end
end

% Calculate pressure profile
profiles.p(1)=namelist.p0;
for i=2:nstep
    profiles.p(i)=profiles.p(i-1)*exp(-const.g*(profiles.z(i)-profiles.z(i-1))/(const.Rd*(profiles.T(i-1)+profiles.T(i)+2*273.15)/2));
end

% Calculate profile of deltaO in vapour
profiles.deltaOv=(profiles.deltaHv-profiles.dexcv)/8.;


% Define or calculate the altitude where the hydrometeor is released
% ------------------------------------------------------------------

% Calculate excess moisture profile (measured moisture divided by the
% saturation vapour pressure over ice) [kg/kg].
profiles.exm=(profiles.h.*satvappress(profiles.T)./satvappress_ice(profiles.T)-1).*...
    profiles.p./(const.Rd*(profiles.T+273.15)).*profiles.h.*0.622.*satvappress(profiles.T)./...
    (profiles.p-satvappress(profiles.T));
profiles.exm(profiles.exm<0)=0; % Set exm to 0 if negative -> no negative exm values allowed

% Define starting/formation height of the hydrometeor
if namelist.z_form==0
    % Take the highest z
    startheight=z(end);
elseif namelist.z_form==-1
    % Take the lowermost point where excess moisture is above 0
    startheight=z(find(profiles.exm>0,1));
else
    % Take the namelist value
    startheight=namelist.z_form;
end

% Initiate output array: Every z between ground and starting height
outarray=z(z<=startheight);

% Define the length of the integration step
if namelist.dt~=0 % time integration
    dt=namelist.dt;
    intsteps=5000/namelist.dt; % number of integration steps
else % height integration
    dt=namelist.dz;
    intsteps=length(0:namelist.dz:startheight); % number of integration steps
end

% Integration to determine diam(z) and m(z)
% -----------------------------------------
height=zeros(intsteps,1);   % altitude
temptend=zeros(intsteps,1); % temperature tendency
Td=zeros(intsteps,1);       % drop temperature
masstend=zeros(intsteps,1); % mass tendency
m=zeros(intsteps,1);        % drop mass
diam=zeros(intsteps,1);     % drop diameter
equih=ones(intsteps,1);     % equivalent relative humidity
                            % (relative humidity with respect to Td
                            % instead of T)

% Determine starting diameter
if strcmp(diam_input{2},'start') % Starting diameter is given as input
    diam(1)=diam_input{1};
elseif strcmp(diam_input{2},'end')
    % Final diameter on the ground is given as input -> determine starting
    % diameter by iteratively adjusting it until the final diameter is matched
    diam(1)=find_input_diameter(diam_input{1},startheight,profiles,const,namelist);
else
    fprintf('Wrong information for drop diameter!\n')
    return;
end

% Define starting values (index 1) of height, mass and drop temperature
height(1)=startheight;
m(1)=1000/6*pi*diam(1)^3;
Td(1)=get_profile_value(profiles,height(1),'T');

% Start integration
i=1;
while i<intsteps
    i=i+1;
    % Get values of T and p for the previous height (i-1)
    [T,p]=get_profile_value(profiles,height(i-1),'T','p');

    % Calculate height of the current integration step
    if namelist.dt==0 % height integration
        height(i)=height(i-1)-namelist.dz;
    else % time integration: Calculate new height by using the fall velocity of the drop
        if Td(i-1)<=0 && namelist.solidterminalvelocity==1
            % Use terminal velocity of a solid hydrometeor if Td<=0°C and
            % if this is allowed by the namelist switch
            height(i)=height(i-1)-terminal_velocity_solid(diam(i-1))*dt;
        else
            % Use terminal velocity of a liquid hydrometeor (with the drop
            % size of the previous integration step)
            height(i)=height(i-1)-terminal_velocity(diam(i-1),p,T)*dt;
        end
    end

    % If the drop falls below z(1): Reduce dt, such that the drop falls
    % exactly to z(1)
    if height(i)<z(1)
        height(i)=z(1);
        dt=(height(i-1)-height(i))/terminal_velocity(diam(i-1),p,T);
    end

    % Determine average values of T, p and h between height(i) and
    % height(i-1)
    [T,p,h]=get_profile_value(profiles,mean([height(i),height(i-1)]),'T','p','h');

    % Calculate drop temperature, using drop diameter and drop temperature
    % from the previous integration step (i-1)
    temptend(i)=drop_T_tendency(diam(i-1),T,Td(i-1),p,h,const,namelist);
    Td(i)=Td(i-1)-dt*temptend(i);
    % Calculate drop mass, using drop diameter from the previous
    % integration step (i-1) and mean drop temperature between (i) and (i-1)
    masstend(i)=drop_mass_tendency(diam(i-1),T,mean([Td(i),Td(i-1)]),p,h,const,namelist);
    m(i)=m(i-1)-dt*masstend(i);

    % Calculate new diameter
    diam(i)=(6*m(i)/(pi*1000))^(1/3);

    % Calculate equivalent relative humidity
    if Td(i)>=0
        equih(i)=1+h-satvappress(Td(i))*(T+273.15)/(satvappress(T)*(Td(i)+273.15));
    else
        equih(i)=1+h-satvappress_ice(Td(i))*(T+273.15)/(satvappress(T)*(Td(i)+273.15));
    end

    % Prepare arrays for output
    if diam(i)<3e-5 % threshold for complete drop evaporation (to avoid numerical
                    % instabilities): set remaining variables to 0 or NaN
        if namelist.dt==0 % height integration
            m(i:intsteps)=0;
            diam(i:intsteps)=0;
            Td(i+1:intsteps)=NaN;
            masstend(i+1:intsteps)=NaN;
            temptend(i+1:intsteps)=NaN;
            equih(i+1:intsteps)=NaN;
            height(i+1:intsteps)=NaN;
            intsteps=i-1;
        else % time integration
            m(i)=0;
            diam(i)=0;
            Td(i)=NaN;
            intsteps=i-1;
        end
        % reduce arrays
        height=height(1:i);
        temptend=temptend(1:i);
        Td=Td(1:i);
        masstend=masstend(1:i);
        m=m(1:i);
        diam=diam(1:i);
        equih=equih(1:i);
        break;
    elseif height(i)==z(1) % drop arrived on ground. Keep last value for interpolation
        intsteps=i;
        height=height(1:i);
        temptend=temptend(1:i);
        Td=Td(1:i);
        masstend=masstend(1:i);
        m=m(1:i);
        diam=diam(1:i);
        equih=equih(1:i);
        break;
    end
    % Raise error if maximum number of integration steps is reached ->
    % avoid too long or infinite running time of the model
    if namelist.dt~=0
        if i==intsteps
            fprintf('Error! Maximum number of integration steps reached before drop evaporated completely or arrived on the ground!\n')
        end
    end
end

% Define output (profiles)
profiles.z_intsteps=height(1:intsteps);
if namelist.dt~=0 % time integration
    % interpolate to the desired output grid: (0:dt:formheight)
    profiles.diam=interp1(height(1:intsteps),diam(1:intsteps),outarray);
    profiles.Td=interp1(height(1:intsteps),Td(1:intsteps),outarray);
    profiles.m=interp1(height(1:intsteps),m(1:intsteps),outarray);
    profiles.masstend=interp1(height(1:intsteps),masstend(1:intsteps),outarray);
    profiles.temptend=interp1(height(1:intsteps),temptend(1:intsteps),outarray);
    profiles.equih=interp1(height(1:intsteps),equih(1:intsteps),outarray);
    profiles.height=outarray;
else
    % invert profiles -> index 1 is on the groud
    profiles.diam=fliplr(diam);
    profiles.Td=fliplr(Td);
    profiles.m=fliplr(m);
    profiles.masstend=fliplr(masstend);
    profiles.temptend=fliplr(temptend);
    profiles.height=fliplr(height);
    profiles.equih=fliplr(equih);
    if ~all(profiles.height==outarray)
        fprintf('Error! Interpolation grid and integration steps dz do not match!\n')
    end
end

% Integration of the isotopic composition of the hydrometeor
% ----------------------------------------------------------
% deltaHp: delta2H of the hydrometeor
% deltaOp: delta2O of the hydrometeor
% deltaHp_eq: delta2H of the equilibrium vapour from the hydrometeor
% deltaOp_eq: delta2H of the equilibrium vapour from the hydrometeor
% dexcp: d-excess of the hydrometeor
% dexcp_eq: d-excess of the equilibrium vapour from the hydrometeor

% Diagnose the isotopic composition of the hydrometeor from the above
% profiles
[deltaHp,deltaOp,deltaHp_eq,deltaOp_eq,profiles]=calc_isotopes(profiles,namelist,const,outarray);
dexcp=deltaHp-8*deltaOp;
dexcp_eq=deltaHp_eq-8*deltaOp_eq;


% Define output
% -------------

profiles.z_out=profiles.height;
profiles.deltaHp=deltaHp;
profiles.dexcp=dexcp;
profiles.deltaHp_eq=deltaHp_eq;
profiles.dexcp_eq=dexcp_eq;
profiles.Tdelta=profiles.Td-get_profile_value(profiles,outarray,'T'); % Temperature difference between drop and environment
profiles.evapfrac=(max(profiles.m)-profiles.m)./max(profiles.m); % Evaporated mass fraction (current mass relative to starting mass)
end
