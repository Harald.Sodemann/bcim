function diff=diffusion_coefficient(T,p)
% Temperature in [°C], pressure in [Pa]

% After Hall and Pruppacher (1976):
T=T+273.15;
diff=0.211e-4*(101325./p).*(T./273.15).^1.94;

% Monteith and Unsworth (1990):
%diff=21.2e-6*(1+0.007*T);

% units of m^2/s
end