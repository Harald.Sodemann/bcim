function aH=alphaH(T)
% Equilibrium fractionation factor over liquid for 2H
% Input: T in [°C.]

% Majoube 1971
aH=exp(24844./((T+273.15).^2)-76.248./(T+273.15)+0.05261);

% Merlivat and Nief 1967
%aH=exp(15013./((T+273.15).^2)-0.0945);
end