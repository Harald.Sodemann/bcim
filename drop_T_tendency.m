function dTddz=drop_T_tendency(D,T,Td,p,h,const,namelist)
% Function to calculate the drop temperature change due to
% condensation/evaporation and heat diffusion
% Inputs:
% D         drop diameter in [m]
% T         environmental temperature in [°C]
% Td        drop temperature in [°C]
% p         pressure in [Pa]
% h         relative humidity []
% const     struct with some constants
% namelist  struct with different switches
%
% Output:
% dTddz     drop temperature change during the given time step

% Check if drop is allowed to have a different temperature than the
% environment. Exit function if not
if namelist.drop_T==0
    dTddz=0;
    return;
end

% Determine if result has to be divided by the terminal velocity (in case
% of height steps as integration (dz) or not (time integration)
if namelist.dt==0 % Height integration
    tvel=terminal_velocity(D,p,T); % dm/dt*dz/dt=dm/dz
else % time integration
    tvel=1; % do not divide by the terminal velocity -> dm/dt
end

% Check if drop is liquid -> due to different saturation vapour pressure
% and latent heat release
if Td>0  % drop is liquid
    % Temperature change due to evaporation
    evapdT=-12*mass_vent_coeff(D,T,p,h)*diffusion_coefficient(T,p)*...
        latheat_evap(Td)/(D^2*1000*const.cw*tvel*const.Rw)*...
        (h*satvappress(T)/(T+273.15)-satvappress(Td)/(Td+273.15));
    % Temperature change due to heat diffusion
    adapdT=-12*heat_vent_coeff(D,T,p,h)*therm_cond(T,h,p)/...
        (D^2*1000*const.cw*tvel)*(T-Td);
    % Check if both temperature changes are enabled
    if strcmp(namelist.temptendswitch,'both')
        dTddz=evapdT+adapdT; % both temperature changes are enabled
    elseif strcmp(namelist.temptendswitch,'evap')
        dTddz=evapdT; % only change due to evaporation is enabled
    else
        dTddz=adapdT; % only change due to heat diffusion is enabled
    end
else
    % Temperature change due to evaporation
    evapdT=-12*mass_vent_coeff(D,T,p,h)*diffusion_coefficient(T,p)*...
        latheat_sublim(Td)/(D^2*1000*const.ci*tvel*const.Rw)*...
        (h*satvappress(T)/(T+273.15)-satvappress_ice(Td)/(Td+273.15));
    % Temperature change due to heat diffusion
    adapdT=-12*heat_vent_coeff(D,T,p,h)*therm_cond(T,h,p)/...
        (D^2*1000*const.cw*tvel)*(T-Td);
    % Check if both temperature changes are enabled
    if strcmp(namelist.temptendswitch,'both')
        dTddz=evapdT+adapdT; % both temperature changes are enabled
    elseif strcmp(namelist.temptendswitch,'evap')
        dTddz=evapdT; % only change due to evaporation is enabled
    else
        dTddz=adapdT; % only change due to heat diffusion is enabled
    end
end
        
end