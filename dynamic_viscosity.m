function dynvisc=dynamic_viscosity(T)
% Function to calculate the dynamic viscosity
% T:    Temperature [°C]

T=T+273.15;

% Rogers and Yau, 1989
%dynvisc=1.72e-5*(393/(T+120))*(T/273.15)^(3/2);

% Sutherland 1893
dynvisc=1.458e-6*T^(3/2)/(T+110.4);
end