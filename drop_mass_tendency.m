function dmdz=drop_mass_tendency(D,T,Td,p,h,const,namelist)
% Function to calculate the drop mass change due to
% condensation/evaporation
% Inputs:
% D         drop diameter in [m]
% T         environmental temperature in [°C]
% Td        drop temperature in [°C]
% p         pressure in [Pa]
% h         relative humidity []
% const     struct with some constants
% namelist  struct with different switches
%
% Output:
% dmdz     drop mass change during the given time step

% Determine if result has to be divided by the terminal velocity (in case
% of height steps as integration (dz) or not (time integration)
if namelist.dt==0 % Height integration
    tvel=terminal_velocity(D,p,T); % dm/dt*dz/dt=dm/dz
else
    tvel=1; % do not divide by the terminal velocity -> dm/dt
end

% Check if drop is liquid -> due to different saturation vapour pressures
if Td>0
    dmdz=-2*pi*D*mass_vent_coeff(D,T,p,h)*... % Equation (3.11) in Graf (2017)
        diffusion_coefficient(T,p)/...
        (const.Rw*tvel)*(h*satvappress(T)/...
        (T+273.15)-satvappress(Td)/(Td+273.15));
else
    dmdz=-2*pi*D*mass_vent_coeff(D,T,p,h)*...
        diffusion_coefficient(T,p)/...
        (const.Rw*tvel)*(h*satvappress(T)/...
        (T+273.15)-satvappress_ice(Td)/(Td+273.15));
end

% Check if mass increased and if this is allowed
if dmdz<0 && namelist.massincrease==0
    dmdz=0; % Set to 0 if mass increase is not allowed
end

end
