function aO=alphaO(T)
% Equilibrium fractionation factor over liquid for 18O
% Input: T in [°C.]

% Majoube 1971
aO=exp(1137./((T+273.15).^2)-0.4156./(T+273.15)-0.0020667);

end