function L=latheat_evap(T)
% Function to calculate the latent heat of evaporation
% T:	Temperature [°C]

% Rogers and Yau (1989)
L=2500800-2360.*T+1.6.*T.^2-0.06.*T.^3;% For -25°C. < T < 40°C.

%L=(2.501-0.02361*T)*10^6; % For -25°C. < T < 40°C.

end
