# BCIM

**Below-cloud Interaction Model (BCIM)**

A model to simulate the isotopic evolution of a single hydrometeor falling from the cloud through a vertical air column to the ground

*Developed and tested by Pascal Graf, 2016 - 2018*

For detailed documentation with examples, see [the wiki pages documentation.](https://git.app.uib.no/Harald.Sodemann/bcim/-/wikis/Documentation)

**Reference:**

Graf, P., Wernli, H., Pfahl, S., and Sodemann, H.: A new interpretative framework for below-cloud effects on stable water isotopes in vapour and rain, Atmos. Chem. Phys., 19, 747–765, https://doi.org/10.5194/acp-19-747-2019, 2019.
