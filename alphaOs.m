function aOs=alphaOs(T)
% Equilibrium fractionation factor over solid for 18O
% Input: T in [°C.]

% Referenz: Majoube 1971b
aOs=exp(11.839./(T+273.15)-0.028224);

end