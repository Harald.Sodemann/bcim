function [T,h,deltaH,dexc,other]=rayleigh_ascent(T0,h0,p0,deltaH0,dexc0,z)
% Calculate a Rayleigh fractionation profile of an air parcel ascending
% along a vertical array z
% Inputs:
% z:    array of height levels z(i) along which the air parcel ascends,
%       where z(1) is the lowest level
% T0:   initial temperature of the air parcel (at z(1))
% h0:   initial relative humidity
% p0:   initial pressure
% deltaH0:  initial deltaH
% dexc0:    initial dexc
%
% Outputs:
% Profiles of T, h, deltaH and dexc
% Additional profiles can be switched on and are defined in the variable "other"

% Define constants

RHvsmow=155.76E-6; % isotope ratio (hdo/h2o) of VSMOW2
ROvsmow=2005.20E-6; % isotope ratio (hh18o/h2o) of VSMOW2
g = 9.81; % gravity constant
cp = 1005; % specific heat of dry air at constant pressure
Rd = 287; % specific gas constant for dry air

% calculate initial deltaO, RH and RO
deltaO0=(deltaH0-dexc0)/8;
RH0=RHvsmow*(deltaH0/1000+1);
RO0=ROvsmow*(deltaO0/1000+1);

% Define array of zeros for some variables
[T,h,RH,RO,p,rv,q]=deal(zeros(size(z)));

% Set initial values
T(1)=T0; % temperature
h(1)=h0; % relative humidity
RH(1)=RH0; % isotope ratio of hdo/h2o
RO(1)=RO0; % isotope ratio of h18o/h2o
p(1)=p0; % pressure
rv(1)=mass_mix_ratio(T0,h0,p0); % rv: mass mixing ratio of vapour in dry air
q(1)=rv(1)/(1+rv(1)); % q: specific humidity [kg/kg]

% Start ascent
for i=2:length(z)
    
    % Determine lapse rate Gamma
    if h(i-1)<1 % Air is below saturation
        gamma= g / cp;
    else % Air is abve saturation
        gamma= g / cp *...
            (1 + latheat_evap(T(i-1))*rv(i-1)/(Rd*(T(i-1)+273.15)))/...
            (1 + latheat_evap(T(i-1))^2*rv(i-1)*0.622/(cp*Rd*(T(i-1)+273.15)^2));
    end
    % Calculate new temperature from lapse rate and height difference
    T(i)=T(i-1) - gamma * (z(i) - z(i-1));
    % Calculate pressure from hydrostatic balance
    p(i)=p(i-1)*((T(i)+273.15)/(T(i-1)+273.15))^(g/(Rd*gamma));
    
    % Determine saturation vapour pressure:
    % Function uses supersaturation with respect to ice Si. Si is cubically
    % interpolated between -23 and 0°C and weighted with a factor fc
    % For details see Section 3.1.1 in Graf 2017 (Diss ETH No 24777)
    if T(i)>=0 
        fc=1;
        Si=satvappress(T(i))/satvappress_ice(T(i));
    elseif T(i)<-23
        fc=0;
        %Si=1.02-0.0038*T(i); % Petit et al. (1991)
        Si=1-0.004*T(i); % Jouzel and Merlivat (1984)
    else
        fc=interp1([-30,-23,0,10],[0,0,1,1],T(i),'pchip');
        Si=interp1([-30,-23,0,10],[1.12,1.092,1.001,0.9084],T(i),'pchip');
    end
    esat=fc*satvappress(T(i))+(1-fc)*Si*satvappress_ice(T(i));
    
    rv(i)=0.622*esat/(p(i)-esat); % rv: mass mixing ratio of vapour in dry air
    h(i)=rv(i-1)/mass_mix_ratio(T(i),1,p(i)); % relative humidity
    if h(i)>1 % set relative humidity back to 1. Rayleigh ascent assumes that
              % excess moisture rains out immediately
        h(i)=1;
    end
    rv(i)=mass_mix_ratio(T(i),h(i),p(i)); % calculate new rv (in case of rainout)
    q(i)=rv(i)/(1+rv(i)); % calculate new specivic humidity
    
    % Calculate effective fractionation factors (using fractionation over
    % liquid for >0°C, fractionation over ice plus kinetic fractionation
    % for T>-23°C. and a cubic interpolation in between)
    alphaHeff=fc*alphaH(T(i))+(1-fc)*alphaHs(T(i))*alphaHk(T(i));
    alphaOeff=fc*alphaO(T(i))+(1-fc)*alphaOs(T(i))*alphaOk(T(i));
    
    % Calculate new isotopic ratios of vapour
    if h(i)==1 % Moisture was lost: Use Eq. (4) of Stewart (1975)
        RH(i)=RH(i-1)*(q(i)/q(i-1))^(alphaHeff-1);
        RO(i)=RO(i-1)*(q(i)/q(i-1))^(alphaOeff-1);
    else % No moisture was lost: keep old ration
        RH(i)=RH(i-1);
        RO(i)=RO(i-1);
    end
end

% Calculate deltas and dexcess from isotopic ratios
deltaH=(RH./RHvsmow-1)*1000;
deltaO=(RO./ROvsmow-1)*1000;
dexc=deltaH-8*deltaO;

% Define other output variables or profiles
other.p=p;
other.deltaO=deltaO;
other.rv=rv;
other.q=q;
other.RH=RH;
other.RO=RO;
end

