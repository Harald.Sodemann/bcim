function rv=mass_mix_ratio(T,h,p)
% Function to calculate the mass mixing ratio of vapour in dry air
% T:    Temperature [°C]
% h:	Relative humidity []
% p:	Pressure [Pa]

fact=p./(p+satvappress(T).*(h-1));
e=h.*satvappress(T).*fact;
rv=0.622.*e./(p-e);

end