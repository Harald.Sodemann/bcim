function vt=terminal_velocity(diameter,p,T)
% Function to calculate the terminal velocity of a liquid hydrometeor
% Input:    Drop diameter in m
%           Pressure in Pa
%           Temperaure in °C.
% Output: terminal velocity in m/s


% % Pruppacher and Klett
% aD=9.65;    % Coefficient A in m/s
% bD=9.8;     % Coefficient B in m/s
% cD=600;     % Coefficient C in 1/m
% vt=aD-bD*exp(-cD*diameter);


% Foote and DuToit, 1969
% Includes correction for lower air density aloft
A0=943;
a=1.77;
n=1.147;
p0=101300;
T0=293.15;

vt=A0./100.*(1-exp(-(diameter.*1000./a).^n)).*(p0.*(T+273.15)./(p.*T0)).^(0.4);

if vt<0
    vt=0;
end

% Langleben 1954:
% Terminal velocity of dendrite snowflakes with "melted diameter" of 0.4 - 3.5 mm:
% Original equation: vt=160*diameter^0.31 with diameter in cm and vt in cm/s
% vt=1.60.*(diameter.*100).^0.3

end