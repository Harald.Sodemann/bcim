function rho=air_density(T,press,rh)
% Function to calculate the density of moist air
% T:        Temperature [°C]
% press:    Pressure  [Pa]
% rh:       Relative humidity []

Rd=287.058; % Specific gas constant of dry air

rho=1/(Rd*(T+273.15))*(press-0.378*rh*satvappress(T));

end