function Dm=dmfromrr(RR)
% Function to calculate the mass weighted mean diameter assuming a
% Marshall-Palmer drop size distribution
if length(RR)==1 && RR==0
    Dm=0;
else
    D=logspace(-4,-2,51)';
    
    for i=1:length(D)-1
        Dbin(i,1)=(D(i+1)+D(i))/2;
        Dwid(i,1)=D(i+1)-D(i);
    end
    
    N0=8e3;
    lambda=4.1e3*RR.^-0.21;
    Nd=N0*exp(Dbin.*(-lambda));
    
    M4=sum(Nd.*Dbin.^4.*Dwid*1000,1);
    M3=sum(Nd.*Dbin.^3.*Dwid*1000,1);
    
    Dm=M4./M3;
    if length(RR)>1
        Dm(RR==0)=0;
    end
end
end
