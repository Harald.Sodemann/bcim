function k=therm_cond(T,h,p)
% Function to calculate the thermal conductivity of moist air
% T:    Temperature [°C]
% h:    Relative humidity []
% p:    Pressure [Pa]

% After Pruppacher and Klett (2010)

ka=(5.69e-5+0.017e-5*T)*418.4; %418.4: Factor for conversion from cal/cm^-1 to J/m
kv=(3.78e-5+0.020e-5*T)*418.4;
gamma1=1.17;
gamma2=1.02;
rv=mass_mix_ratio(T,h,p);
xv=rv./(rv+0.6226);
% (13-18) in Pruppacher and Klett (2010)
k=ka.*(1-(gamma1-gamma2.*kv./ka).*xv);

end