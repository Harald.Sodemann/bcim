function fv=mass_vent_coeff(diameter,T,press,rh,varargin)
% Function to calculate the mass ventilation coefficient
% diameter: diameter [m]
% T:        Temperature [°C]
% press:    pressure [Pa]
% rh:       relative humidity []
% varargin: water species
%           empty:      normal water or H2O
%           'heavyH':   HDO
%           'heavyO':   HH18O

if length(varargin)==1
    if strcmp(varargin{1},'heavyH')
        heavyfact=1/1.0251;
    elseif strcmp(varargin{1},'heavyO')
        heavyfact=1/1.0285;
    end
else
    heavyfact=1;
end

% Pruppacher and Klett, 2010
diff=diffusion_coefficient(T,press)*heavyfact;
rho=air_density(T,press,rh);
Re=terminal_velocity(diameter,press,T)*diameter*rho/dynamic_viscosity(T);
Sc=dynamic_viscosity(T)/(rho*diff);
ScRe=Sc^(1/3)*sqrt(Re);
if ScRe<1.4
    fv=1.0 + 0.108 * ScRe^2;
else
    fv=0.78 + 0.308 * ScRe;
end

end