function esat=satvappress_ice(T)
% Calculate saturation vapour pressure over ice
% Temperature in °C.
% Result in Pa

% from Murphy and Koop (2005)

% Coefficients
A=9.550426;
B=5723.265;
C=3.53068;
D=0.00728332;

T=T+273.15; % Change temperature to Kelvin

esat=exp(A-B./T+C*log(T)-D*T); % for T > 110 K


end